// por Henrique Freitas Lopes

let titleText = document.getElementById("titleText");
let sex = 0; // 0 homem; 1 mulher; 2 não binário
const welcomeTexts = ["Bem vindo ao P.I.E.R","Bem vinda ao P.I.E.R","Bem vinde ao P.I.E.R"]
const welcomeTypes = ["\0", " Works", " Demos"];

if(document.cookie === "") sex = 0;
else{
    const cs = document.cookie.split(';'), csf = cs[1].replace(' sex=', '');
    sex = parseInt(csf);
    lazyToWriteThisTwoTimes();
}

function onClickChangeTextSex(){
    switch(sex){
        case 0:
            sex = 1;
            break;
        case 1:
            sex = 2;
            break;
        case 2:
            sex = 0;
            break;
        default:
            sex = 0;
    }
    lazyToWriteThisTwoTimes();
    cookieCreate('sex',sex.toString());
}

function scrollDownDisplay(thingId, btnId){
    let thingToScroll = document.getElementById(thingId);
    let bntToStyle = document.getElementById(btnId);
    bntToStyle.style.borderTopLeftRadius = 5 + "px";
    bntToStyle.style.borderTopRightRadius = 5 + "px";
    if(thingToScroll.style.maxHeight == thingToScroll.scrollHeight + "px"){
        thingToScroll.style.maxHeight = 0;
        bntToStyle.classList.toggle('displaying');
        bntToStyle.style.borderBottomLeftRadius = 5 + "px";
        bntToStyle.style.borderBottomRightRadius = 5 + "px";
        bntToStyle.style.transitionDelay = "0.2s";
    }
    else{
        thingToScroll.style.maxHeight = thingToScroll.scrollHeight + "px";
        bntToStyle.classList.toggle('displaying');
        bntToStyle.style.transitionDelay = "0.0s";
        bntToStyle.style.borderBottomLeftRadius = 0 + "px";
        bntToStyle.style.borderBottomRightRadius = 0 + "px";
    }
}

function cookieCreate(dataName, dataValue){
    document.cookie = `${dataName}=${dataValue}; expires=${oneMonthInAdvance()}; path=/`
}

function oneMonthInAdvance(){
    const currentDate = new Date();
    const dateTest = new Date(currentDate.getFullYear(), currentDate.getMonth()+1);
    return dateTest.toUTCString();
}

function lazyToWriteThisTwoTimes(){
    switch(window.location.pathname){
        case "/index.html":
            titleText.innerText = welcomeTexts[sex] + welcomeTypes[0];
            break;
        case "/pierWoks.html":
            titleText.innerText = welcomeTexts[sex] + welcomeTypes[1];
            break;
        case "/pierDemos.html":
            titleText.innerText = welcomeTexts[sex] + welcomeTypes[2];
            break;
        default:
            titleText.innerText = welcomeTexts[sex] + welcomeTypes[0];
    }
}
